# Projet TAA GLI

Réalisé par Fahim MERZOUK,

Il y a que ce README.md qui est fait pour les 2 parties (2 répos git différents)

# Lancement de l'application:

## Backend

* Tout d'abord il faut faire un "mvn clean" puis un "mvn install", lancer MyApplication.java qui se trouve dans le package my_app.

* Le backend de l'application se lance depuis l'IDE de votre choix, le serveur se lance sur le port 8080.

## Frontend 

* Pour lancer le frontend, il faut taper la commande : "npm start" ou bien "ng serve --proxy-config proxy.config.json".

* le serveur se lance sur le port 4200.

# Travail effectué

## Backend 

* Le backend de mon application est assez riche avec des DAO(repository), REST, JPA, Services, api externe de météo, Swagger pour documenter mon api, JavaMailSender de spring(envoi chaque mardi à 2h du matin un mail fonctionnel), spring security(faute de temps, je me suis bloqué dessus quelques temps puis je suis passé à la vérification de l'authentification moi meme)... etc

* J'ai essayé d'utiliser le plein de technos vues en cours, mais faute de temps et étant monome j'ai pas eu le temps de déployer mon application sur des conteneurs docker (je le ferai un peu après ce rendu).

## Frontend

* Le frontend est développé en angular 6 avec typescript.
* J'ai utilisé quelques modules de primeng qui fournissent des templates html tout prets à l'emploi.
* j'ai mis en place un proxy qui redirige toutes les requetes commençant par /api/* au backend pour éviter le CORS.
* j'ai utilisé le systeme de routage d'angular pour naviguer entre les pages.
* L'api de météo utilisé donne un résultat instantanné, la prévision météo du week-end est payante (j'avoue que je suis un peu radin ;) ).

* L'application est sur http://localhost:4200/ 

### Scénario d'utilisation

* on arrive sur login, si on a un compte, on sign in et on tombe sur le dashboard de l'utilisateur courrant où c'est affiché:
ses sports, sa ville, le temps qu'il fait dans sa ville, les sports qui lui sont proposés en fonction du temps qu'il fait dans
sa ville et les sports qu'il pratique (couvert ou non).

* si on a pas de compte, il y a une page de création de compte, on rentre le nom prénom email password, on selectionne la ville d'adresse et les sports qu'on pratique. L'utilisateur est ajouté à la base de données et le routage le mène vers son dashboard.

* Après avoir créé un compte, un mail personnalisé sera envoyé chaque mardi à 2h du matin pour proposer des sports à pratiquer en fonction de la météo et des sports choisis lors de l'inscription. 


# Base de données
* J'utilise une base de données mysql en local, localhost:3306/TAA_WE_PROJECT avec user="taa" et password="Taa_2018".
* Ma base de donnée contient 3 tables : personne, adresse, sport.

# Les services
* Après avoir lancer l'application, si vous voulez utiliser ses services, j'ai utilisé Swagger pour que L'API soit découverable en lecture sur l'url :("http://localhost:8080/swagger-ui.html").
* L'api Rest a été implémentée avec le framework sping avec les annotations spring qui conviennent. 

