package my_app.weather;

import com.mashape.unirest.http.exceptions.UnirestException;

import my_app.domain.Weather;
import my_app.domain.WeatherUtils;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/WeatherService")
public class WeatherService {
	
	@Autowired
	private WeatherUtils weatherUtils;
	
	@RequestMapping( method = RequestMethod.GET,
			params = {"_city"})
	public Weather getOne(@RequestParam(value = "_city") String city) throws UnirestException {
		JSONObject w = weatherUtils.getWeather(city);
		
		//récupère que les champs de la réponse de l'api météo qui m'interessent
		
		Weather weather = new Weather(
				w.getString("description"),
				w.getString("main")
						);

		return weather;
	}

}
