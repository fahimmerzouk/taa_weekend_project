package my_app.weather;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import my_app.domain.WeatherUtils;

@Component("WeatherUtils")
public class WeatherUtilsImpl implements WeatherUtils {


	final private String baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
	final private String appid = "&appid=ed1fdd09023260d8d2b6c5056f2a02e4";
	
	@Override
	public String makeUrl(String city) {
		return baseUrl + city + appid;
	}

	@Override
	public String makeUrl(String city, String country) {
		String url = baseUrl + city + "," + country + appid;
		return url;
	}

	@Override
	public JSONObject getWeather(String city) throws UnirestException {
		String url = makeUrl(city);
		HttpResponse<JsonNode> jsonResponse = Unirest.get(url).header("accept", "application/json").asJson();
		JsonNode responseBody = jsonResponse.getBody();
		JSONObject results = responseBody.getObject();
		JSONObject weather = results.getJSONArray("weather").getJSONObject(0);
		return weather;
	}
}
