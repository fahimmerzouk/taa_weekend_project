package my_app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import my_app.mail.ScheduleEmail;

@Configuration
@EnableScheduling
public class SpringConfig {
	 @Bean
     public ScheduleEmail task() {
         return new ScheduleEmail();
     }
	 
	 
}
