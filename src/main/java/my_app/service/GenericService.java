package my_app.service;

import java.util.List;
import org.springframework.web.bind.annotation.*;


public interface GenericService<T> {
	
	    //test
		@GetMapping("/test")
	    public String helloWorld();
	    
		@RequestMapping( method = RequestMethod.GET,
				params = {"_id"})
	    public T getOne(Long id);
	    
		@GetMapping("/getAll")
	    public List<T> getAll();
	    
		@PostMapping("/add")
	    public T addOne(T p);
		
		@PostMapping("/addMany")
		void addMany(List<T> lObject);
		
		@RequestMapping( method = RequestMethod.DELETE,
				params = {"_id"})
		void deleteOneById(long id);
		
		@DeleteMapping("/deleteMany")
		void delete(List<T> lObject);
}
