package my_app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import io.swagger.annotations.Api;
import my_app.domain.Person;
import my_app.repository.PersonDAO;

@RestController
@RequestMapping("/api/PersonService")
@Api(value="PersonServiceApi", produces=MediaType.APPLICATION_JSON_VALUE)
public class PersonService implements GenericService<Person>{
	
	@Autowired
	private PersonDAO personneDAO; 
	
	
	@Override
	public String helloWorld() {
		return "Hello world!";
	}
	
	@Override
	public Person getOne(@RequestParam(value = "_id") Long id) {
		Optional<Person> p = personneDAO.findById(id);
		return p.get();
	}
	
	@RequestMapping(value = "/email", method = RequestMethod.GET,
			params = {"_email"})
	public Person getByEmail(@RequestParam(value = "_email") String email) {
		Person p = personneDAO.findByEmail(email);
		return p;
	}
	
	
	@Override
	public List<Person> getAll() {
		return personneDAO.findAll();
	}
	
	
	@Override
	public Person addOne(@RequestBody Person p) {		
		return personneDAO.save(p);
	}

	@Override
	public void addMany(@RequestBody List<Person> lObject) {
		for (Person p : lObject){
      personneDAO.save(p);
  }		
	}

	@Override
	public void deleteOneById(@RequestParam(value = "_id")long id) {
		personneDAO.deleteById(id);		
	}


	@Override
	public void delete(@RequestBody List<Person> lObject) {
		for (Person p : lObject){
            personneDAO.deleteById(p.getId());
        }		
	}
}

