package my_app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import my_app.domain.Sport;
import my_app.repository.SportDAO;;


@RestController
@RequestMapping("/api/SportService")
public class SportService  implements GenericService<Sport>{
	
	@Autowired
	private SportDAO sportDAO; 
	
	
	@Override
	public String helloWorld() {
		return "Hello world!";
	}
	
	@Override
	public Sport getOne(@RequestParam(value = "_id") Long id) {
		Optional<Sport> p = sportDAO.findById(id);
		return p.get();
	}
	
	
	@Override
	public List<Sport> getAll() {
		return sportDAO.findAll();
	}
	
	
	@Override
	public Sport addOne(@RequestBody Sport s) {		
		return sportDAO.save(s);
	}

	@Override
	public void addMany(@RequestBody List<Sport> lObject) {
		for (Sport s : lObject){
			sportDAO.save(s);
  }		
	}

	@Override
	public void deleteOneById(@RequestParam(value = "_id")long id) {
		sportDAO.deleteById(id);		
	}


	@Override
	public void delete(@RequestBody List<Sport> lObject) {
		for (Sport s : lObject){
			sportDAO.deleteById(s.getId());
        }		
	}
	






    

}


