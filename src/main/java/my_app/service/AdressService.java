package my_app.service;

import java.util.List;
import java.util.Optional;

//import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import my_app.domain.Adress;
import my_app.repository.AdressDAO;;


@RestController
@RequestMapping("/api/AdressService")
public class AdressService  implements GenericService<Adress>{
	
	@Autowired
	private AdressDAO adresseDAO; 
	
	
	@Override
	public String helloWorld() {
		return "Hello world!";
	}
	
	@Override
	public Adress getOne(@RequestParam(value = "_id") Long id) {
		Optional<Adress> a = adresseDAO.findById(id);
		return a.get();
	}
	
	
	@Override
	public List<Adress> getAll() {
		return adresseDAO.findAll();
	}
	
	
	@Override
	public Adress addOne(@RequestBody Adress a) {		
		return adresseDAO.save(a);
	}

	@Override
	public void addMany(@RequestBody List<Adress> lObject) {
		for (Adress a : lObject){
			adresseDAO.save(a);
  }		
	}

	@Override
	public void deleteOneById(@RequestParam(value = "_id") long id) {
		adresseDAO.deleteById(id);		
	}


	@Override
	public void delete(@RequestBody List<Adress> lObject) {
		for (Adress a : lObject){
			adresseDAO.deleteById(a.getId());
        }		
	}
	






    

}


