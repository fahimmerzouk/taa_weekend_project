package my_app.domain;

//import org.apache.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sport")
public class Sport {
		
	long id;

	String nomSport;
	
	int couvert; // 1 si c'est couvert, 0 sinon
	

	//private final static Logger logger = Logger.getLogger(Adress.class);	
	
	public Sport() {}


	public Sport(long id, String nomSport, int couvert ) {
		super();
		this.id = id;
		this.nomSport = nomSport;
		this.couvert = couvert;
	}

	public Sport(String nomSport) {
		super();
		this.nomSport = nomSport;
	}

	@Id
	@GeneratedValue
	@Column(nullable = false , name = "sport_id")
	public long getId() {
		//logger.debug("getId :");
		return id;
	}

	public void setId(long id) {
		//logger.debug("setId :"+id);
		this.id = id;
	}

	public String getNomSport() {
		//logger.debug("getNomSport :");
		return nomSport;
	}

	public void setNomSport(String nomSport) {
		//logger.debug("setNomSport :" +nomSport);
		this.nomSport = nomSport;
	}
	

	public int getCouvert() {
		return couvert;
	}


	public void setCouvert(int couvert) {
		this.couvert = couvert;
	}


	@Override
	public String toString() {
		return "Sport [id=" + id + ", nomSport=" + nomSport + "]";
	}

}