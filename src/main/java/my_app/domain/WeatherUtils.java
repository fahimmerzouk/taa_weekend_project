package my_app.domain;

import org.json.JSONObject;

import com.mashape.unirest.http.exceptions.UnirestException;


public interface WeatherUtils {


	public String makeUrl(String city) ;

	public String makeUrl(String city, String country);

	public JSONObject getWeather(String city) throws UnirestException ;

}
