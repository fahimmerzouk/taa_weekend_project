package my_app.domain;

import java.util.List;
//import org.apache.log4j.Logger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.jboss.logging.Logger;

@Entity
@Table(name = "personne")
public class Person {
	
	long person_id;
	
	
	String name, firstname, email, password;
	
	Adress adresse ; 
	
	List<Sport> sports ;
	
	private final static Logger logger = Logger.getLogger(Adress.class);	
	
	public Person() {
		super();
	}

	public Person(String name, String firstname, String email, Adress adresse, List<Sport> sports) {
		super();
		this.name = name;
		this.firstname = firstname;
		this.email = email;
		this.adresse = adresse;
		this.sports = sports;
	}
	
	public Person(String email,String name, String firstname,  String password, Adress adresse, List<Sport> sports) {
		super();
		this.name = name;
		this.firstname = firstname;
		this.email = email;
		this.password = password;
		this.adresse = adresse;
		this.sports = sports;
	}

	@Id
	@GeneratedValue
	@Column(nullable = false, name="person_id")
	public long getId() {
		logger.debug("getId :");
		return person_id;
	}

	public void setId(long person_id) {
		//logger.debug("setId :" + person_id);
		this.person_id = person_id;
	}
	
	@Column(length=100,nullable = false)
	public String getName() {
		logger.debug("getName :" );
		return name;
	}
	
	
	public void setName(String name) {
		//logger.debug("setName :" +name);
		this.name = name;
	}
	
	@Column(length=100,nullable = false)
	public String getFirstname() {
		logger.debug("getFirstname :" );
		return firstname;
	}

	public void setFirstname(String firstname) {
		logger.debug("setFirstname :" + firstname );
		this.firstname = firstname;
	}
	
	@Column(length=100,nullable = false , unique=true)
	public String getEmail() {
		logger.debug("getEmail :" );
		return email;
	}
	
	public void setEmail(String email) {
		logger.debug("setEmail :" + email );
		this.email = email;
	}
	
	
	public String getPassword() {
		logger.debug("getPassword :" );
		return password;
	}

	public void setPassword(String password) {
		logger.debug("setPassword :" +password);
		this.password = password;
	}

	@OneToOne
	public Adress getAdresse() {
		logger.debug("getAdresse :" );
		return adresse;
	}

	public void setAdresse(Adress adresse) {
		logger.debug("setAdresse :" + adresse);
		this.adresse = adresse;
	}
	@ManyToMany
	@JoinTable(name = "person_sports", 
    joinColumns = { @JoinColumn(name = "person_id") }, 
    inverseJoinColumns = { @JoinColumn(name = "sport_id") })
	public List<Sport> getSports() {
		logger.debug("getSports :" );
		return sports;
	}

	public void setSports(List<Sport> sports) {
		logger.debug("setSports :" +sports);
		this.sports = sports;
	}

	@Override
	public String toString() {
		return "Person [person_id=" + person_id + ", name=" + name + ", firstname=" + firstname + "]";
	}


	
	
}