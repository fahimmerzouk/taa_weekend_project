package my_app.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

//import org.apache.log4j.Logger;

@Entity
@Table(name = "adresse")
public class Adress{

	  private long id;
	  
	  private String adresse;
	  

	//private final static Logger logger = Logger.getLogger(Adress.class);
	public Adress() {}


	public Adress(String adresse) {
		this.adresse = adresse;
	}
	
	@Id
	@GeneratedValue
	@Column(nullable = false, name = "adress_id")
	public long getId() {
		//logger.debug("get ID :");
		return this.id;
	}

	public void setId(long id) {
		//logger.debug("setId : " + id);
		this.id = id;
	}
	
	public String getAdresse() {
		//logger.debug("getAdresse :");
		return adresse;
	}

	public void setAdresse(String adresse) {
		//logger.debug("setAdresse : " + adresse);
		this.adresse = adresse;
	}
	

	@Override
	public String toString() {
		return "Adress [id=" + id + ", adresse=" + adresse  + "]";
	}

}