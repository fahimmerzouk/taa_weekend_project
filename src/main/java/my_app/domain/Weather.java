package my_app.domain;

public class Weather {
	String description;
	String main;
	
	public Weather() {
		super();
	}
	
	public Weather(String description, String main) {
		super();
		this.description = description;
		this.main = main;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}



	
}
