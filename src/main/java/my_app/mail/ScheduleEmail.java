package my_app.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import my_app.domain.Person;
import my_app.domain.Sport;
import my_app.domain.WeatherUtils;
import my_app.repository.PersonDAO;

@Component
public class ScheduleEmail {
	
	@Autowired
	public JavaMailSender emailSender;
	
	@Autowired 
	public PersonDAO personDao;
	
	@Autowired
	public WeatherUtils weather;
	
	//rfzdfdfcsdqdcqs
	//sdfsfs
	
	//"* * * * * *" chaque seconde pour tester et ne pas attendre
	//seconde minute heure jourMois mois jourSemaine
	//chaque mardi à 2h du matin
	//@Scheduled(cron = "* * * * * *") //chaque seconde
	@Scheduled(cron = "0 0 2 * * TUE")
	
	public void work() throws UnirestException {
		String subject = "TAA WE project";
		
		
		List<Person> listePersonnes = personDao.findAll();
		List<Sport> sportsProposes = new ArrayList<Sport>();
		for (Person p : listePersonnes) {
			String w = weather.getWeather(p.getAdresse().getAdresse()).getString("main");
			for (Sport s : p.getSports()) {
				if((w != "Rain" && w != "Snow") || (s.getCouvert() == 1))
					sportsProposes.add(s);	
			}
			String text ="Bonjour, \n En fonction des sports que vous pratiquez, "
					+ "et la météo dans votre ville ("+ w 
					+ ") \n voici les sports que vous pouvez pratiquez :\n" ;
			for (Sport s : sportsProposes) {
				text += "- " + s.getNomSport() + "\n";
			}
			sendSimpleMessage(p.getEmail(), subject, text);
		}
		
		

	}
	public void sendSimpleMessage(
        String to, String subject, String text) {
        
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(to); 
        message.setSubject(subject); 
        message.setText(text);
        emailSender.send(message);
    }
	    
    @Bean
	 public JavaMailSender getJavaMailSender() {
	     JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	     mailSender.setHost("smtp.gmail.com");
	     mailSender.setPort(587);
	      
	     mailSender.setUsername("taa2018.fmerzouk@gmail.com");
	     mailSender.setPassword("Taa_2018");
	      
	     Properties props = mailSender.getJavaMailProperties();
	     props.put("mail.transport.protocol", "smtp");
	     props.put("mail.smtp.auth", "true");
	     props.put("mail.smtp.starttls.enable", "true");
	     props.put("mail.debug", "true");
	      
	     return mailSender;
	 }
}
