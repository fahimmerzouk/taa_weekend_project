package my_app.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import my_app.domain.*;



@Transactional
public interface PersonDAO extends JpaRepository<Person, Long>{	
	@Query ("select p from Person p Where p.email = ?1")
    public Person findByEmail (String email);
}
