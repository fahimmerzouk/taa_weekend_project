package my_app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import my_app.domain.Adress;


@Transactional
public interface AdressDAO extends JpaRepository<Adress, Long>{
	
}
