package my_app.repository;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import my_app.domain.*;

@Transactional
public interface SportDAO extends JpaRepository<Sport, Long>{


}
