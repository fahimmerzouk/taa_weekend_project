TRUNCATE adresse;
TRUNCATE sport;

LOAD DATA LOCAL INFILE '/home/fmerzouk/eclipse-workspace/taa_weekend_project/src/main/resources/cities.csv'
INTO TABLE adresse
FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(adress_id, adresse);

LOAD DATA LOCAL INFILE '/home/fmerzouk/eclipse-workspace/taa_weekend_project/src/main/resources/sports.csv'
INTO TABLE sport
FIELDS TERMINATED BY ','
    ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(sport_id, nom_sport, couvert);
